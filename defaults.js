function defaults(testObject,defaultProps){
    if(!testObject){
        return {};
    }
    if(!defaultProps){
        return testObject;
    }
    flag = true;
    for(const index in testObject){
        if((typeof index==="undefined") || (typeof testObject[index]==="undefined")){//Here,we are checking the undefined cases
              flag=false   
        }
    }
    if(flag){
        return testObject;
    }else{
        for(const index in defaultProps){
            testObject[index]=defaultProps[index];
        }
        return JSON.stringify(testObject);
    }   
}
module.exports=defaults;