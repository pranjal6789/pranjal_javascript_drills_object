function mapObject(testObject,cb){
    if(!testObject){
        return [];
    }
    if(typeof cb==="undefined"){
        return testObject;
    }
    var arr=[];
    for(const index in testObject){
        h1=cb(testObject[index]);
        testObject[index]=h1;
    }
    return JSON.stringify(testObject);
}
module.exports=mapObject;