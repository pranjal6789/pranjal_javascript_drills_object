function invert(testObject){
    if(!testObject){
        return [];
    }
    var resultObj={};
    for(const index in testObject){
        resultObj[testObject[index]]=index;
    }
    const final_result=JSON.stringify(resultObj);
    return final_result;
}

module.exports=invert;