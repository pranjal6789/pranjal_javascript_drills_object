function pairs(testObject){
    var result_arr=[];
    if(!testObject){
        return [];
    }
    for(const index in testObject){
        result_arr.push([index,testObject[index]]);
    }
    return JSON.stringify(result_arr);
}
module.exports=pairs;