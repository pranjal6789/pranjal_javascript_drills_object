function values(testObject){
    if(!testObject){
        return [];
    }
    var result_arr=[];
    for(const index in testObject){
        result_arr.push(testObject[index]);
    }
    return JSON.stringify(result_arr);
}

module.exports=values;